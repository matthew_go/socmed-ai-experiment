window.fbAsyncInit = function() {
    // FB JavaScript SDK configuration and setup
    FB.init({
      appId      : '152439012028341', // FB App ID
      cookie     : true,  // enable cookies to allow the server to access the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.8' // use graph api version 2.8
    });

    // Check whether the user already logged in
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            var access_token =   FB.getAuthResponse()['accessToken'];
            var contentObj = {'user_access_token': access_token}
            alert(contentObj)
            var ajax_result = ''
            $.ajax({
                url:"/set_user_access_token/",
                type: "POST",
                data: contentObj,
                dataType: "json",
                success:function(data){
                    console.log(data.success);
                    ajax_result = 'Success';
                },
                complete:function(){
                    console.log('complete');
                    ajax_result = 'Complete';
                },
                error:function (xhr, textStatus, thrownError){
                    console.log(thrownError);
                    console.log(obj);
                    ajax_result = 'Failed';
                }
            });
            document.getElementById('accessToken').innerHTML = 'Access Token: ' + access_token;
            //display user data
            getFbUserData();
        }
    });
};

// Load the JavaScript SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Facebook login with JavaScript SDK
function fbLogin() {
    console.log('Called fbLogin()')
    FB.login(function (response) {
        if (response.authResponse) {
            var access_token =   FB.getAuthResponse()['accessToken'];
            document.getElementById('accessToken').innerHTML = 'Access Token: ' + access_token;
            // Get and display the user profile data
            var contentObj = {'user_access_token': access_token}
            alert(contentObj)
            var ajax_result = ''
            $.ajax({
                url:"/set_user_access_token/",
                type: "POST",
                data: contentObj,
                dataType: "json",
                success:function(data){
                    console.log(data.success);
                    ajax_result = 'Success';
                },
                complete:function(){
                    console.log('complete');
                    ajax_result = 'Complete';
                },
                error:function (xhr, textStatus, thrownError){
                    console.log(thrownError);
                    console.log(obj);
                    ajax_result = 'Failed';
                }
            });
            getFbUserData();
        } else {
            document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
        }
    }, {scope: 'email, read_page_mailboxes, manage_pages'});
}

// Fetch the user profile data from facebook
function getFbUserData(){
    FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
    function (response) {
        document.getElementById('fbLink').setAttribute("onclick","fbLogout()");
        document.getElementById('fbLink').innerHTML = '<img src="' + logout_button + '"/>';
        document.getElementById('status').innerHTML = 'Thanks for logging in, ' + response.first_name + '!';
        document.getElementById('userData').innerHTML = '<p><b>FB ID:</b> '+response.id+'</p><p><b>Name:</b> '+response.first_name+' '+response.last_name+'</p><p><b>Email:</b> '+response.email+'</p><p><b>Gender:</b> '+response.gender+'</p><p><b>Locale:</b> '+response.locale+'</p><p><b>Picture:</b> <img src="'+response.picture.data.url+'"/></p><p><b>FB Profile:</b> <a target="_blank" href="'+response.link+'">click to view profile</a></p>';
    });
}

// Logout from facebook
function fbLogout() {
    FB.logout(function() {
        document.getElementById('fbLink').setAttribute("onclick","fbLogin()");
        document.getElementById('fbLink').innerHTML = '<img src="fblogin.png"/>';
        document.getElementById('userData').innerHTML = '';
        document.getElementById('status').innerHTML = 'You have successfully logout from Facebook.';
    });
}