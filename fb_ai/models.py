from django.db import models

# Create your models here.
class AccessToken(models.Model):
    user_access_token = models.CharField(max_length=300)
    page_access_token = models.CharField(max_length=300)

class Conversation(models.Model):
    conversation_id = models.CharField(max_length=300, unique=True)