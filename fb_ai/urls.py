from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^fb/login', views.login, name='login'),
    url(r'^set_user_access_token', views.set_user_access_token, name='set_user_access_token'),
    url(r'^crawl_conversation_ids', views.crawl_conversation_ids, name='crawl_conversation_ids'),
    url(r'^count_conversation_ids', views.count_conversation_ids, name='count_conversation_ids')
]