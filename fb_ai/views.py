from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from fb_ai.models import AccessToken, Conversation
import requests, json, traceback
from django.db import IntegrityError

# Create your views here.
def login(request):
    return render(request, 'fb_ai/login.html', {})

@csrf_exempt
def set_user_access_token(request):
    user_access_token = request.POST.get('user_access_token', None)

    response = requests.get('https://graph.facebook.com/me/permissions?access_token=' + user_access_token)
    print(response.content)

    response = requests.get('https://graph.facebook.com/citihub?fields=access_token&access_token='+user_access_token)
    data = json.loads(response.content)

    page_access_token = data.get('access_token')

    print(page_access_token)

    AccessToken.objects.all().delete()
    at = AccessToken(user_access_token=user_access_token, page_access_token=page_access_token)
    at.save()

    return render(request, 'fb_ai/home.html')

def crawl_conversation_ids(request):
    print('crawlin')
    access_token = AccessToken.objects.first()
    print(access_token.page_access_token, access_token.user_access_token)

    url = 'https://graph.facebook.com/citihub/conversations?fields=access_token&access_token=' + access_token.page_access_token
    next = collect_conversation_ids(url)
    print("Next", next)
    while next is not None:
        next = collect_conversation_ids(next)

    return render(request, 'fb_ai/home.html', {'count': Conversation.objects.count()})

def count_conversation_ids(request):
    return render(request, 'fb_ai/home.html', {'count': Conversation.objects.count()})

def collect_conversation_ids(url):

    response = requests.get(url)
    content = json.loads(response.content)
    print(json.dumps(content, indent=4))
    for entry in content.get('data'):
        c = Conversation(conversation_id=entry.get('id'))
        try:
            c.save()
        except IntegrityError:
            #print(traceback.format_exc())
            print('integrity error')
    next = content.get('paging').get('next') if 'paging' in content and 'next' in content.get('paging') else None
    return next